This page describes the error and status code the server can send back to a client interrogating the server API

| Error Code | Http Status Code | Meaning |
|---|---|---|
| ACTION_NEEDED | 403 | The user needs to perform an action on its bank website. |
| BANK_ALREADY_EXISTS | 409 | The access already exists: the same login/bank credentials are already used within Kresus. |
| EXPIRED_PASSWORD | 403 | The user needs to update its access password on its bank website. |
| GENERIC_EXCEPTION | 500 | An unindentified error occured. |
| INVALID_PARAMETERS | 400 | Some credentials are missing to connect to the bank's website (usually when other inputs than login/password have to be provided). |
| INVALID_PASSWORD | 401 | The password is incorrect, the user has to update it. |
| NO_ACCOUNTS | 500 | No accounts could be retrieved on the the bank website. This is usually the consequence of a Weboob module not up to date. The user should try to update the modules through the appropriate button. |
| NO_PASSWORD | 400 | The password was not transferred to Kresus/Weboob. The user should input it again. |
| UNKNOWN_WEBOOB_MODULE | 500 | The requested module is unknown to weboob. This is either due to a non-existing module, or a missing dependency. |
| WEBOOB_NOT_INSTALLED | 500 | Weboob does not seem to be installed. The admin should reinstall it or install the appropriate dependencies. |
| INTERNAL_ERROR | 500 | An unknown error occured. |
| DISABLED_ACCESS | 403 | The access the user is trying to fetch operations/accounts is disabled, they should enable it first. |