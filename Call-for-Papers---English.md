## Title

Kresus, a libre personal finance manager

A libre, open-source, self-hostable, privacy aware, personal finance manager for all

## Abstract / Description

Banking information is utterly personal, but we still give away this very valuable data to private companies like Bankin and others. Kresus is a personal finance manager web application that is entirely free and open-source, which one can self-host to keep control on their privacy while still getting insightful information out of it.

The following items will be discussed:

- why and how did we start the project
- how does it work
- what can it do for you, what are the main features, and a demo that will probably break in real-time
- the security aspects of it
- a hat tip to the community and contributors, as well as a call to action