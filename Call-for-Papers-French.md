## Titre

Kresus, un gestionnaire libre de finances personnelles

## Description

Les informations bancaires sont extrêmement personnelles et donnent beaucoup de détails intimes sur notre vie privée. Kresus, une application web entièrement libre de gestion de finances personnelles, auto-hébergeable à souhait, permet d'en apprendre plus sur ses finances : centralisation de tous les comptes bancaires en un seul lieu, catégorisation des transactions bancaires et création de graphiques personalisés, budgétisation mensuelle, récupération automatique des transactions et envoi de notifications par emails générées par des évènements particuliers. Et tout ça en respectant votre  vie privée et en vous laissant un contrôle total sur vos données, contrairement à des alternatives propriétaires et payantes comme Bankin, Linxo, etc.

Venez découvrir cette application et ses principes, comment l'utiliser, comment l'installer facilement, pour ensuite en discuter ensemble, ainsi que des manières de contribuer à l'élaboration de ce logiciel (*spoiler alert*, il n'y a pas besoin de s'y connaître en logiciel pour ça !).