(écrit courant 2020)

# Notre vision

Kresus se veut être un logiciel de gestion de finances personelles **libre, auto-hébergeable, simple, et agréable,** pour aider ses utilisateurices à être plus conscient de l'état de leurs finances, comment celles-ci évoluent au cours du temps, voire de les aider à trouver des aspects dans lesquels iels pourraient faire des économies selon les objectifs qu'iels se sont fixés.

* libre / auto-hébergeable : parce que les données bancaires sont trop personnelles pour être échangées ou revendues par des entreprises tierces, il est nécessaire que les utilisateurices gardent le contrôle sur la manière dont leurs données bancaires sont traitées et hébergées. Cela passe donc par un code libre pour pouvoir auditer la manière dont le logiciel fonctionne, ainsi que la possibilité d'auto-héberger simplement le logiciel pour pouvoir l'utiliser et garder le contrôle sur le matériel serveur le faisant tourner.
* simple : Kresus veut remplir les usages de la majorité des utilisateurices, être compréhensible et utilisable par toutes, fournir le minimum d'informations maximalement utiles.
* agréable : les finances personnelles peuvent être un sujet sensible, parfois angoissant, parfois stressant. Kresus veut faire que la navigation dans ses finances personnelles soit détendue, sans jugement, sans indicateur de pression, sans stress.

# Notre feuille de route

Cette feuille de route vise à indiquer quelles sont les fonctionnalités sur lesquelles nous désirons nous concentrer, c'est-à-dire celles sur lesquelles nous voulons travailler en priorité. Cela ne veut pas dire que rien d'autre ne sera implémenté, mais il s'agit d'un plan pour aller vers une première version majeure de Kresus.

## Améliorer l'expérience utilisateur.ice du projet

N'ayant eu peu de ligne directrice en termes d'expérience utilisateur.ice (UX) au cours du temps, ni n'ayant beaucoup recherché de contributions en UX au cours du temps, l'UI et l'UX du projet manquent parfois de consistence et de cohérence, ayant été développé à plusieurs mains.

Nous pensons pouvoir améliorer ceci, en :

* attirant explicitement des gens passionés par l'UI et l'UX, en expérimentant des nouveaux modes de contribution via des médias différents de ceux utilisés par les programmeurses,
* mettant un place un ensemble d'éléments de design cohérents
* implémentant des composants réutilisables qui incarnent ce design
* réimaginant les pages et formulaires de Kresus.

Par exemple :

* Nous voudrions quitter le monde des fenêtre modales pour revenir à des écrans complets qui ne superposent pas de l'information à une autre information.
* Nous souhaiterions revoir l'ensemble des préférences, afin de les rendre lisibles et compréhensibles par toustes.
* Nous imaginons de nouvelles manières de présenter les budgets, afin de les rendre compréhensible et moins austères. [(En cours)](https://community.kresus.org/t/fonctionnement-actuel-des-budgets-signification-des-couleurs-barres/)

## Implémenter OTP et 2FA proprement

À l'heure actuelle, la 2FA est supportée de manière minimale, sans qu'aucun affichage à l'écran n'indique à l'utilisateurice qu'iel doit vailder la connexion via son app mobile. L'idée ici consiste à améliorer cet état de fait, et à supporter également un aller-retour via la possibilité de remplir des champs de formulaire supplémentaires pour les OTP.

## Faire que la fusion de doublons soit un problème du passé

Il s'agit ici d'implémenter un système capable de détecter et fusionner automatiquement la plupart des doublons, en se souvenant des fusions, pour pouvoir les annuler en cas d'erreur de l'algorithme. Une fusion erronnée ne doit pas avoir lieu deux fois.

En comparant le solde réel sur chaque compte avec le solde calculé par Kresus, il serait même possible d'évaluer exhaustivement les paires de transaction à fusionner, pour retomber sur le solde réel.

## L'auto-catégorisation des transactions, enfin

L'auto catégorisation se ferait potentiellement de deux manières :

* via un système expert, c'est-à-dire des règles entrées manuellement par les utilisateurices
* via du *machine learning*, à base d'apprentissage sur les catégorisations manuelles, et un système de validation simple à utiliser.

## Faire de Kresus un logiciel multi-utilisateurices

Cela consiste à mettre en place une authentification côté client qui interagisse proprement avec le serveur, et à modifier les quelques endroits dans le serveur où des préférences devraient en fait être compartimentées par utilisateurice (par exemple : la langue de traduction).
